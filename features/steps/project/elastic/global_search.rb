require_dependency Rails.root.join('spec', 'support', 'stub_configuration')

class Spinach::Features::GlobalSearch < Spinach::FeatureSteps
  include SharedAuthentication
  include SharedPaths
  include SharedProject
  include SharedElastic
  include StubConfiguration

  before do
    stub_ee_application_setting(elasticsearch_search: true, elasticsearch_indexing: true)

    Gitlab::Elastic::Helper.create_empty_index
  end

  after do
    Gitlab::Elastic::Helper.delete_index

    stub_ee_application_setting(elasticsearch_search: false, elasticsearch_indexing: false)
  end

  step 'project has all data available for the search' do
    @project = create :project
    @project.add_master(current_user)

    @issue = create :issue, title: 'bla-bla initial', project: @project
    @merge_request = create :merge_request, title: 'bla-bla initial', source_project: @project
    @milestone = create :milestone, title: 'bla-bla initial', project: @project
  end
end
