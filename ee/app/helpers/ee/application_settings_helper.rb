module EE
  module ApplicationSettingsHelper
    extend ::Gitlab::Utils::Override

    override :visible_attributes
    def visible_attributes
      super + [
        :check_namespace_plan,
        :elasticsearch_aws,
        :elasticsearch_aws_access_key,
        :elasticsearch_aws_region,
        :elasticsearch_aws_secret_access_key,
        :elasticsearch_experimental_indexer,
        :elasticsearch_indexing,
        :elasticsearch_search,
        :elasticsearch_url,
        :geo_status_timeout,
        :help_text,
        :repository_size_limit,
        :shared_runners_minutes,
        :slack_app_enabled,
        :slack_app_id,
        :slack_app_secret,
        :slack_app_verification_token,
        :allow_group_owners_to_manage_ldap,
        :mirror_available
      ]
    end

    def self.repository_mirror_attributes
      [
        :mirror_max_capacity,
        :mirror_max_delay,
        :mirror_capacity_threshold
      ]
    end

    def self.external_authorization_service_attributes
      [
        :external_authorization_service_enabled,
        :external_authorization_service_url,
        :external_authorization_service_default_label
      ]
    end

    def self.possible_licensed_attributes
      repository_mirror_attributes + external_authorization_service_attributes
    end
  end
end
